;;;; -*- Mode: Lisp -*-
;;;;
;;;; graph.lisp --
;;;; Cleaned up version of CAPI plot protocol implementation.
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.


(in-package "CL-PLOT")

(defclass graph ()
  ((name :accessor graph-name
         :initarg :name)
   (datasets :accessor graph-datasets
             :initform ())
   )
  (:default-initargs
   :name (gensym "GRAPH-")))


(defgeneric graph-p (x)
  (:method ((x graph)) t)
  (:method ((x t)) nil))


(defclass graph-2d (graph)
  ((x-labels :accessor x-labels :initarg :x-labels)
   (y-labels :accessor y-labels :initarg :y-labels)
   )
  (:default-initargs :x-labels () :y-labels ()))

(defgeneric graph-2d-p (x)
  (:method ((x graph-2d)) t)
  (:method ((x t)) nil))


(defclass graph-3d (graph-2d)
  ((z-labels :accessor z-labels :initarg :z-labels))
  (:default-initargs :z-labels ())
  )

(defgeneric graph-3d-p (x)
  (:method ((x graph-3d)) t)
  (:method ((x t)) nil))


;;;===========================================================================
;;; Protocol.

;;; Factory function.

(defgeneric make-graph (type &rest keys &key &allow-other-keys))


;;; Extremes.

(defgeneric graph-x-max (graph))
(defgeneric graph-y-max (graph))
(defgeneric graph-z-max (graph))

(defgeneric graph-x-min (graph))
(defgeneric graph-y-min (graph))
(defgeneric graph-z-min (graph))


;;; Dataset handling.

(defgeneric add-dataset (graph dataset &key &allow-other-keys))

(defgeneric delete-dataset (graph dataset &key test key))

(defgeneric graph-n-datasets (graph))


;;; end of file -- graph.lisp --
