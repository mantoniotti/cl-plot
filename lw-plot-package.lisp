;;; -*- Mode: Lisp -*-

(defpackage "CL-PLOT" (:use "COMMON-LISP")
  (:export
   "PLOT")

  (:export
   "DEVICE")

  (:export
   "DATASET"
   "DATASET-2D"
   "DATASET-3D"

   "DATASETP"

   "MAKE-DATASET"

   "DATASET-X-DATA"
   "DATASET-Y-DATA"
   "DATASET-Z-DATA"

   "DATASET-X-MAX"
   "DATASET-Y-MAX"
   "DATASET-Z-MAX"

   "DATASET-X-MIN"
   "DATASET-Y-MIN"
   "DATASET-Z-MIN"
   )
   )

;;; end of file -- cl-plot-package.lisp --
