;;;; -*- Mode: Lisp -*-

;;;; cl-plot.system
;;;; 
;;;; Plotting utilities (mostly for LW).
;;;;
;;;; See the file COPYING for copyright and licensing information.

(mk:defsystem "CL-PLOT"
  :author "Marco Antoniotti <mantoniotti@common-lisp.net>"
  :licence "BSD"

  :documentation "A Common Lisp plotting library (mostly LW CAPI FTTB)."

  :binary-pathname "_fasl/"

  :components ("cl-plot-package"
               (:file "dataset"
                :depends-on ("cl-plot-package"))
               (:file "dataset-implementation"
                :depends-on ("dataset"))
               (:file "graph"
                :depends-on ("cl-plot-package"))
               (:file "graph-implementation"
                :depends-on ("graph"))

               ;; "graphs-implementation"

               (:file "device"
                :depends-on ("cl-plot-package"))
               (:file "plot-protocol"
                :depends-on ("cl-plot-package"))
               (:file "points"
                :depends-on ("cl-plot-package"))

               (:module "impl-dependent"
                :components (
                             #+lispworks
                             (:module "lw-capi"
                              :components (:serial
                                           (:file "cl-plot-capi-pkg")
                                           (:file "logging")
                                           (:file "canonical-transforms")

                                           (:file "capi-device")

                                           (:file "plot-frame")
                                           (:file "plot-area")
                                           (:file "plot-element")
                                           (:file "plot-axes")
                                           (:file "plot-object")
                                           (:file "boredered-output-pane")
                                           
                                           (:file "lw-plot-interface")
					   (:file "plot-protocol-impl-capi")
                                           (:file "plot-setup")
                                           (:file "gestures")
                                           (:file "drawing")
                                           (:file "plot-interface-functions")
                                           ;; (:file "test")
                                           ;; Come back later to add tests.
                                           ))
                             )
                :depends-on ("cl-plot-package"))
               )
  :depends-on (#+split-sequence
               "split-sequence"

               #-split-sequence
               (:foreign-system "split-sequence")

               #+clad
               "CLAD"

               #-clad
               (:foreign-system "clad")
               )
  )

;;;; end of file -- cl-plot.system
