;;; -*- Mode: Lisp -*-

(in-package "LW-PLOT")

(defstruct (point (:constructor nil)))

(defstruct (point-2d (:include point)
                     (:conc-name point-)
                     (:constructor make-point-2d (x y)))
  (x 0)
  (y 0))

(defstruct (point-3d (:include point-2d)
                     (:conc-name point-)
                     (:constructor make-point-3d (x y z)))
  (z 0))

;;;---------------------------------------------------------------------------
;;; Implementation

(defun make-point (x y &optional (z 0 z-supplied-p))
  (if z-supplied-p
      (make-point-3d x y z)
      (make-point-2d x y)))

;;; end of file -- points.lisp --
