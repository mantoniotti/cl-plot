;;;; -*- Mode: Lisp -*-

;;;; plot-protocol.lisp
;;;;
;;;; Plotting utilities for LW; "plot protocol" definition and
;;;; implementation.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT")

(defvar *standard-plot-device* *standard-output*)

;;; (defgeneric plot (data &rest keys &key type device &allow-other-keys))


(defgeneric plot-data (current-plot-output plot-designator
                                           &key
                                           &allow-other-keys))


(defgeneric add-plot (new-plot-designator &optional
                                          current-plot-output
                                          &key
                                          &allow-other-keys))

(defgeneric remove-plot (plot-designator &optional current-plot-output
                                         &key
                                         &allow-other-keys))

(defgeneric clear (&optional current-plot-output))

(defgeneric new-plot-display (sink-type &rest keys
                                        &key
                                        &allow-other-keys))


;;;---------------------------------------------------------------------------
;;; Implementation.

(defmethod new-plot-display ((s stream) &rest keys &key &allow-other-keys)
  (declare (ignore keys))
  s)


(defmethod new-plot-display :around (sink-type &rest keys &key &allow-other-keys)
  (setf *standard-plot-device* (call-next-method)))


#|
(defmethod new-plot-display ((s (eql :figure)) &rest keys &key &allow-other-keys)
  (make-instance 'cl-plot-capi:plot-interface))
|#


(defmacro plotting ())

(defun plot (data &rest keys
                  &key
                  (kind :2d)
                  (type :line)
                  (device *standard-plot-device*)
                  &allow-other-keys)
  (apply #'plot-data device (make-shaped-dataset data type kind) keys)
  )


#|
(defmethod plot ((data list) &rest keys
                 &key
                 (kind :2d)
                 (type :line)
                 (device *standard-plot-device*)
                 &allow-other-keys)
  (declare (ignore kind))
  (remf keys :kind)
  (remf keys :type)
  (remf keys :device)
  (case type
    (:line
     (apply #'plot-data device (make-shaped-dataset data :line :2d) keys))
    (:horizontal-bar (plot-data device (make-dataset++++ data :line) ))
    (:vertical-bar (plot-data device (make-dataset++++ data :line) ))
    (:stack (plot-data device (make-dataset++++ data :line) ))
    (:area (plot-data device (make-dataset++++ data :line) ))
    (otherwise (plot-data device (make-dataset++++ data type) ))
    ))
|#


(defun line-plot (data &rest keys
                       &key (device *standard-plot-device*)
                       &allow-other-keys)
  (declare (ignore device))

  (remf keys :type)
  (apply #'plot data :type :line keys))


(defun bar (data &rest keys
                 &key (device *standard-plot-device*)
                 &allow-other-keys)
  (declare (ignore device))

  (remf keys :type)
  (apply #'plot data :type :vertical-bar keys))


(defun barh (data &rest keys
                  &key (device *standard-plot-device*)
                  &allow-other-keys)
  (declare (ignore device))

  (remf keys :type)
  (apply #'plot data :type :horizontal-bar keys))


(defun stack (data &rest keys
                   &key (device *standard-plot-device*)
                   &allow-other-keys)
  (declare (ignore device))

  (remf keys :type)
  (apply #'plot data :type :stack keys))


(defun area (data &rest keys
                  &key (device *standard-plot-device*)
                  &allow-other-keys)
  (declare (ignore device))

  (remf keys :type)
  (apply #'plot data :type :area keys))


;;; end of file -- plot-protocol.lisp --
