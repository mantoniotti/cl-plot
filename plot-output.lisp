;;; -*- Mode: Lisp -*-

(in-package "CL-PLOT")

(defclass plot-output ()
  ((graphs :accessor plot-output-graphs
           :initform '())
   ))

(defgeneric plot-output-p (x)
  (:method ((x t)) nil)
  (:method ((x plot-output)) t))

(defclass stream-plot-output (plot-output)
  ((stream :accessor stream-plot-output-stream)))


(defclass standard-output-plot-output (stream-plot-output)
  ())


;;;---------------------------------------------------------------------------
;;; Plot Output Protocol.


;;; Plot outputs list

(defgeneric add-plot-output (plot-output-designator))

(defgeneric remove-plot-output (plot-output-designator))

(declaim (ftype (function () plot-output) current-plot-output))

(declaim (ftype (function () list) list-plot-outputs))


;;; Constructor.

(defgeneric make-plot-output (kind &rest keys &key &allow-other-keys))
  



;;; Visuals.

(defgeneric show-plot-output (plot-output))

(defgeneric hide-plot-output (plot-output))

(defgeneric destroy-plot-output (plot-output))


;;; Printing

(defgeneric print-plot-output (plot-output stream))


;;; end of file -- plot-output.lisp --
