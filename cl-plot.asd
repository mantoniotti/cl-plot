;;;; -*- Mode: Lisp -*-

;;;; cl-plot.asd
;;;; 
;;;; Plotting utilities (mostly for LW).
;;;;
;;;; See the file COPYING for copyright and licensing information.

(asdf:defsystem "cl-plot"
  :author "Marco Antoniotti <mantoniotti@common-lisp.net>"
  :licence "BSD"

  :description "A Common Lisp plotting library (mostly LW CAPI FTTB)."

  :serial t
  :components ((:file"cl-plot-package")
               (:file "dataset")
               (:file "dataset-implementation")
               (:file "graph")
               (:file "graph-implementation")

               ;; "graphs-implementation"

               (:file "device")
               (:file "plot-protocol")
               (:file "points")
               (:module "impl-dependent"
                :serial t
                :components (
                             #+lispworks
                             (:module "lw-capi"
                              :serial t
                              :components ((:file "cl-plot-capi-pkg")
                                           (:file "logging")
                                           (:file "canonical-transforms")

                                           (:file "capi-device")

                                           (:file "plot-frame")
                                           (:file "plot-area")
                                           (:file "plot-element")
                                           (:file "plot-axes")
                                           (:file "plot-object")
                                           (:file "bordered-output-pane")

                                           (:file "lw-plot-interface")
					   (:file "plot-protocol-impl-capi")
                                           (:file "plot-setup")
                                           (:file "gestures")
                                           (:file "drawing")
                                           (:file "plot-interface-functions")
                                           ;; (:file "test")
                                           ))
                             )))
  :depends-on ("split-sequence"
               "clad")
  )

;;;; end of file -- cl-plot.asd
