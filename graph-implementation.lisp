;;; -*- Mode: Lisp -*-

;;;; graph-implementation.lisp
;;;
;;;; Plotting utilities for LW; "graphs" implementation.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")

(defmethod make-graph ((type (eql 'graph-2d))
                       &rest keys
                       &key
                       (datasets nil)
                       &allow-other-keys)
  (remf keys :datasets)
  (let ((new-g (apply #'make-instance 'graph-2d keys)))
    (dolist (d datasets new-g)
      (apply #'add-dataset new-g d keys))))


(defmethod make-graph ((type (eql :graph-2d))
                       &rest keys
                       &key
                       &allow-other-keys)
  (apply 'make-graph 'graph-2d keys))


(defmethod make-graph ((type (eql :2d-graph))
                       &rest keys
                       &key
                       &allow-other-keys)
  (apply 'make-graph 'graph-2d keys))



(defmethod add-dataset ((g graph) (d dataset) &key &allow-other-keys)
  (setf (graph-datasets g) (nconc (graph-datasets g) (list d))))

(defmethod delete-dataset ((g graph) dataset
                           &key
                           (test #'eql)
                           (key #'identity))
  (setf (graph-datasets g)
        (delete dataset (graph-datasets g) :test test :key key)))

(defmethod clear-graph ((g graph))
  (setf (graph-datasets g) ()))



(defmethod graph-x-max ((g graph))
  (loop for d in (graph-datasets g)
        maximize (dataset-x-max d)))

(defmethod graph-y-max ((g graph))
  (loop for d in (graph-datasets g)
        maximize (dataset-y-max d)))

(defmethod graph-z-max ((g graph))
  (loop for d in (graph-datasets g)
        maximize (dataset-z-max d)))


(defmethod graph-x-min ((g graph))
  (loop for d in (graph-datasets g)
        minimize (dataset-x-min d)))

(defmethod graph-y-min ((g graph))
  (loop for d in (graph-datasets g)
        minimize (dataset-y-min d)))

(defmethod graph-z-min ((g graph))
  (loop for d in (graph-datasets g)
        minimize (dataset-z-min d)))


(defmethod graph-n-datasets ((g graph))
  (list-length (graph-datasets g)))


(defmethod add-dataset ((g graph) (ds array)
                        &key
                        (header-row-p t)
                        (header-column-p t)
                        (column-names (x-labels g))
                        row-names
                        &allow-other-keys)
  (assert (= (array-rank ds) 2)) ; Must be a matrix.
  (let ((start-i (if header-row-p 1 0))
        (start-j (if header-column-p 1 0))
        (r-dim (array-dimension ds 0))
        (c-dim (array-dimension ds 1))
        )
    (loop for i from start-i below r-dim
          collect (make-dataset
                   :name (if row-names
                             (elt row-names i)
                             (aref ds i 0))
                   :y-data (make-array (- c-dim start-j)
                                       :displaced-to ds
                                       :displaced-index-offset (+ (* i c-dim) start-j)))
          into datasets
          finally (dolist (d datasets)
                    (add-dataset g d))
          )
    (setf (x-labels g) column-names)
    (graph-datasets g)))


(defmethod add-dataset ((g graph) (ds vector)
                        &key
                        (column-names (x-labels g))
                        (row-name-p t)
                        row-name
                        &allow-other-keys)
  (let* ((l (length ds))
         (d (make-dataset :name (if row-name-p (aref ds 0) row-name) 
                          :y-data (if row-name-p
                                      (make-array (1- l)
                                                  :displaced-to ds
                                                  :displaced-index-offset
                                                  1)
                                      ds)))
         (cs (if column-names
                 column-names
                 (loop for i from (if row-name-p 1 0) below l
                       collect (format nil "~D" i))))
                 
         )
    (add-dataset g d)
    (setf (x-labels g) cs)
    (graph-datasets g)))


;;; end of file -- graph-implementation.lisp --
