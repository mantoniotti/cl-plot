;;;; -*- Mode: Lisp -*-

(in-package "CL-PLOT")

;;;; dataset.lisp
;;;;
;;;; Plotting utilities for LW; definition of a "dataset".
;;;;
;;;; See the file COPYING for copyright and licensing information.


(defclass dataset ()
  ((name :accessor dataset-name :initarg :name)
   (raw-data :accessor dataset-raw-data :initarg :raw-data)
   (normalized-points :accessor dataset-normalized-points :initform ())
   )
  (:default-initargs :name (gensym "DATASET-") :raw-data '())
  )

(defgeneric datasetp (x)
  (:method ((x dataset)) t)
  (:method ((x t)) nil))


;;; dataset-nd --

(defclass dataset-nd (dataset)
  ()
  )

(defgeneric dataset-nd-p (x)
  (:method ((x dataset-nd)) t)
  (:method ((x t)) nil))


;;; dataset-2d --

(defclass dataset-2d (dataset-nd)
  ((x-data :accessor dataset-x-data :initarg :x-data)
   (y-data :accessor dataset-y-data :initarg :y-data)
   (x-max :reader dataset-x-max)
   (x-min :reader dataset-x-min)
   (y-max :reader dataset-y-max)
   (y-min :reader dataset-y-min)
   )
  (:default-initargs  :x-data '() :y-data '()))

(defgeneric dataset-2d-p (x)
  (:method ((x dataset-2d)) t)
  (:method ((x t)) nil))


;;; dataset-3d --

(defclass dataset-3d (dataset-2d)
  ((z-data :accessor dataset-z-data :initarg :z-data)
   (z-min :accessor dataset-z-min)
   (z-max :accessor dataset-z-max)
   )
  (:default-initargs :z-data '())
  )

(defgeneric dataset-3d-p (x)
  (:method ((x dataset-3d)) t)
  (:method ((x t)) nil))


;;; group-dataset --

(defclass group-dataset (dataset-nd)
  ((x-data :accessor x-data :initarg :x-data)))

(defgeneric group-dataset-p (x)
  (:method ((x group-dataset)) t)
  (:method ((x t)) nil))


;;; bar-dataset --

(defclass bar-dataset (group-dataset) ())

(defgeneric bar-dataset-p (x)
  (:method ((x bar-dataset)) t)
  (:method ((x t)) nil))


;;; area-dataset --

(defclass area-dataset (group-dataset) ())

(defgeneric area-dataset-p (x)
  (:method ((x area-dataset)) t)
  (:method ((x t)) nil))


;;; stack-dataset --

(defclass stack-dataset (group-dataset) ())
;
(defgeneric stack-dataset-p (x)
  (:method ((x stack-dataset)) t)
  (:method ((x t)) nil))


;;;; end of file -- dataset.lisp --
