;;;; -*- Mode: Lisp -*-

;;;; device.lisp
;;;;
;;;; Plotting utilities for LW; "devices" definitions.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")

(defclass device ()
  ()
  )

(defgeneric device-p (x)
  (:method ((x t)) nil)
  (:method ((x device)) t))


(defclass stream-device (device)
  ((stream :accessor stream-device-stream)))

(defgeneric stream-device-p (x)
  (:method ((s stream-device)) t)
  (:method ((s t)) nil))


(defclass standard-output-device (stream-device)
  ())

(defgeneric standard-output-device-p (x)
  (:method ((s standard-output-device)) t)
  (:method ((s t)) nil))


;;;---------------------------------------------------------------------------
;;; Plot Output Protocol.


;;; Plot outputs list

(defgeneric add-device (device-designator))

(defgeneric remove-device (device-designator))

(declaim (ftype (function () device) current-device))

(declaim (ftype (function () list) list-devices))


;;; Constructor.

(defgeneric make-device (kind &rest keys &key &allow-other-keys))
  

;;; Visuals.

(defgeneric show-device (device))

(defgeneric hide-device (device))

(defgeneric destroy-device (device))


;;; Printing

(defgeneric print-device (device stream))


;;; end of file -- device.lisp --
