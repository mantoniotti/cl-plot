;;; -*- Mode: Lisp -*-

;;;; plot-frame.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)


;;;---------------------------------------------------------------------------
;;; Definitions.


;;; plot-frame --

(defclass plot-frame (pinboard-layout) ; Maybe it could also be a PINBOARD-OBJECT.
  ((viewport-x :accessor viewport-x :initform 0.0)
   (viewport-y :accessor viewport-y :initform 0.0)
   (transform :accessor plot-frame-transform
              :initform (gp:make-transform))

   (zoom-in-factor :reader zoom-in-factor
                   :writer set-zoom-in-factor
                   :initform 1.1)
   (zoom-out-factor :reader zoom-out-factor
                    :writer set-zoom-out-factor
                    :initform 0.9)

   ;; CURRENT-ZOOM-FACTOR is the scaling factor currently used.
   ;; CURRENT-SCALE is the 'cumulative' scaling currently in effect.
   (current-zoom-factor :accessor current-zoom-factor :initform 1.0)
   (current-scale :accessor current-scale :initform 1.0)

   (current-operation :accessor current-operation
                      :initform :select
                      ;; One of the operations listed in *plot-frame-operations*
                      )

   (tick-size :accessor tick-size
              :initform 10 ; pixels.
              )
   )
  (:documentation "The Plot Frame Class.")
  )


#+(or lispworks7 lispworks8)
(defmethod initialize-instance :after ((pf plot-frame) &key &allow-other-keys)
  (gp:set-graphics-state pf :scale-thickness nil)
  )


(defgeneric plot-frame-p (x)
  (:method ((x plot-frame)) t)
  (:method ((x t)) nil))




;;;---------------------------------------------------------------------------
;;; Implementation (to be put elsewhere)

(defun plot-frame-device (plot-frame)
  (declare (type plot-frame plot-frame))
  (element-parent plot-frame))

(defun plot-object-frame (plot-object)
  (declare (type plot-object plot-object))
  (element-parent plot-object))

(defun plot-object-device (plot-object)
  (declare (type plot-object plot-object))
  (element-parent (element-parent plot-object)))

(defun capi-device-plot-frame (capi-device)
  (declare (type capi-device capi-device))
  (find-if #'plot-frame-p (layout-description capi-device)))

;;; end of file -- capi-device.lisp --
