;;; -*- Mode: Lisp -*-

;;;; plot-element.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")


;;;---------------------------------------------------------------------------
;;; Definitions.


;;; plot-element --
;;; 
;;; The top class of the things that appear in a plot-frame.

(defclass plot-element (pinboard-object)
  ()
  (:documentation "The Plot Element CLass.

The top class of the things that appear in a plot-frame.
"))


(defgeneric plot-element-p (x)
  (:method ((x plot-element)) t)
  (:method ((x t)) nil))


;;; end of file -- plot-element.lisp --
