;;;; -*- Mode: Lisp -*-

;;;; plot-protocol-impl-capi.lisp --
;;;; Cleaned up version of CAPI plot protocol implementation.
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Notes:
;;;
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;;
;;;; 20220505 MA: cleaned up version.
;;;;
;;;; 20220606 MA: almost working version.
;;;;
;;;; 20221110 MA: cleaned up version; almost working.



;;; A graph contains a list of datasets, which contain its own x and y
;;; (and z) data.
;;; This will allow a plot to include multiple datasets.

;;; Protocol and useful interface.

(defmethod cl-plot::new-plot-display ((s (eql :figure))
                                      &rest keys
                                      &key &allow-other-keys)
  (make-instance 'cl-plot-capi:plot-interface))


(defgeneric resize-plot-device-object (plot-device plot-device-object x y w h))

(defgeneric erase-plot-device (plot-device))

(defgeneric erase-plot-device-object (plot-device plot-device-object x y w h))


;;;---------------------------------------------------------------------------
;;; plot-data
;;;
;;; The basic method is
;;;
;;;    plot-data capi-device graph-2d
;;;
;;; The other methods essentially transform the inputs or select their
;;; inside bits to direct the plotting action.


;;; plot-data (eql T) dataset

(defmethod plot-data ((lw-plot::current-plot-output (eql t)) (d dataset)
                 &key
                 &allow-other-keys)
  ;; 20041015 IAN
  ;; Changed. Added LW-PLOT:: prefix.
  (if (lw-plot::current-plot-output)
      (plot-data (lw-plot::current-plot-output) d)
      (let ((cpo (lw-plot::add-plot-output (lw-plot::make-plot-output))))
        (plot-data cpo d))))


;;; plot-data plot-interface dataset

(defmethod plot-data :after ((plot-interface plot-interface)
                             dataset
                             &rest keys
                             &key)
  (declare (ignore keys))
  (capi:display plot-interface))


(defmethod plot-data ((plot-interface plot-interface)
                      (d2d dataset-2d)
                      &rest keys
                      &key
                      (name "")
                      (x 50)
                      (y 20)
                      (width 300)
                      (height 200)
                      (n-ticks 10)
                      (tick-size 10)
                      )
  (declare (ignore name x y width height n-ticks tick-size))
  (let ((new-graph (make-instance 'graph-2d)))
    (add-dataset new-graph d2d)
    (apply #'plot-data plot-interface new-graph keys)))


(defmethod plot-data :before ((plot-interface plot-interface)
                              (data t)
                              &rest keys
                              &key
                              (name "")
                              (x 50)
                              (y 20)
                              (width 300)
                              (height 200)
                              (n-ticks 10)
                              (tick-size 10)
                              )
  (declare (ignore name x y width height n-ticks tick-size))
  (log-message "plot-data (interface) ~s ~s~%" plot-interface data)
  )


;;; plot-data plot-interface graph

(defmethod plot-data ((plot-interface plot-interface)
                      (g graph-2d)
                      &rest keys
                      &key
                      &allow-other-keys)
  (apply #'execute-with-interface plot-interface
         #'plot-data (plot-interface-device plot-interface) g keys))


;;; plot-data capi-device graph

(defmethod plot-data ((drawing-area capi-device)
                      (g graph-2d)
                      &key
                      (name (graph-name g))
                      (draw-legend-p t)
                      (draw-x-labels-p t)
                      (draw-y-labels-p t)
                      (tick-size 10)
                      (margin 10)
                      &allow-other-keys)
  (declare (ignore name))
  (multiple-value-bind (min-width min-height)
      (get-constraints drawing-area) ; (values 10 10)

    (assert (and min-width min-height) (min-width min-height))

    (with-geometry drawing-area
      (let* ((w (or %width% min-width))
             (h (or %height% min-height))
             (plot-frame-w (* w 0.8))
             (plot-frame-h (* h 0.8))
             (plot-frame-x (/ (- w plot-frame-w) 2.0))
             (plot-frame-y (/ (- h plot-frame-h) 2.0))

             (psw (gp:port-string-width drawing-area "-00000.00"))
             (psh (gp:port-string-height drawing-area "-00000.00"))

             (plot-area-w (- plot-frame-w (* 2 (+ margin psw))))
             (plot-area-h (- plot-frame-h (* 2 (+ margin psh))))
             (plot-area-x (+ margin psw))
             (plot-area-y (+ margin psh))

             (new-tr (gp:copy-transform gp:*unit-transform*))

             (new-plot-frame
              (make-instance 'plot-frame
                             :x plot-frame-x
                             :y plot-frame-y
                             :width plot-frame-w :height plot-frame-h

                             :visible-min-width plot-frame-w
                             :visible-min-height plot-frame-h
                             :visible-max-width plot-frame-w
                             :visible-max-height plot-frame-h

                             :background :white
                             :input-model '(((:button-1 :press) press-button)
                                            ((:button-1 :release) release-button)
                                            )
                             ))

             (new-plot-area
              (make-instance 'plot-area
                             :x plot-area-x
                             :y plot-area-y
                             :background :white
                             :width  plot-area-w
                             :height plot-area-h
                             :internal-min-width  plot-area-w
                             :internal-min-height plot-area-h
                             :internal-max-width  plot-area-w
                             :internal-max-height plot-area-h
                             ))

             
             (new-plot-object
              (make-instance 'plot-object
                             :draw-legend-p   draw-legend-p
                             :draw-x-labels-p draw-x-labels-p
                             :draw-y-labels-p draw-y-labels-p
                             :tick-size tick-size
                             :data g
                             ))

             (plot-object-title
              (make-plot-object-title drawing-area g
                                      plot-frame-x plot-frame-y
                                      (1- plot-frame-w) (1- plot-frame-h)))

             (x-axis-object
              (make-plot-object-axis new-plot-object
                                     g
                                     :x
                                     plot-area-w
                                     plot-area-h
                                     :draw-labels-p draw-x-labels-p
                                     :tick-size tick-size))

             (y-axis-object
              (make-plot-object-axis new-plot-object
                                     g
                                     :y
                                     plot-area-w
                                     plot-area-h
                                     :draw-labels-p draw-y-labels-p
                                     :tick-size tick-size))
             )

        (log-message "plot-data ~S ==> ~S~%"
                     (list %x% %y% %width% %height%)
                     (list plot-frame-x plot-frame-y plot-frame-w plot-frame-h))
        (log-message "plot-data plot-object ~S~%"
                     (list plot-area-x plot-area-y plot-area-w plot-area-h))
        (log-message "plot-data plot-object psw psh ~S~%"
                     (list psw psh))

                     
        (setf (plot-frame-transform new-plot-frame) new-tr)

        (setf (layout-description drawing-area) (list new-plot-frame))

        (setf (layout-description new-plot-frame) (list new-plot-area))

        (manipulate-pinboard new-plot-frame x-axis-object :add-top)
        (manipulate-pinboard new-plot-frame y-axis-object :add-top)

        (manipulate-pinboard new-plot-area new-plot-object :add-top)

        (manipulate-pinboard drawing-area plot-object-title :add-top)

        ))))


(defmethod plot-data :before ((dev capi-device)
                              (data t)
                              &rest keys
                              &key
                              (name "")
                              )
  (declare (ignore name x y width height n-ticks tick-size))
  (log-message "plot-data (capi-device) ~s ~s~%" dev data)
  )


;;; make-plot-object-title

(defmethod make-plot-object-title ((drawing-area capi-device)
                                   (g graph-2d)
                                   x y w h)
  ;; x y w and h are the position and the dimensions of the PLOT-FRAME
  ;; in DRAWING-AREA. 

  (let* ((title (string (name g)))
         (title-psw (gp:port-string-width drawing-area title))
         )
    (make-instance 'item-pinboard-object
                   :text title
                   :x (+ x (- (/ w 2.0) (/ title-psw 2.0)))
                   :y (- y 10)
                   )
    ))
    

;;; resize-plot-device-object

(defmethod resize-plot-device-object ((drawing-area capi-device)
                                      (pbo pinboard-object)
                                      x y w h)
  (declare (ignore x y w h))
  ;; No op.
  ;; Essentially for the item-pinboard-object's now added.
  )


(defmethod resize-plot-device-object ((drawing-area capi-device)
                                      (plot plot-frame)
                                      x y w h)
  (declare (ignore x y w h))

  (let* ((plot-area (first (layout-description plot))) ; Quite an assumption!
         (x-axis (second (layout-description plot))) ; Ditto.
         (y-axis (third (layout-description plot))) ; Ditto.

         (transform (plot-frame-transform plot))
         (dx (transform-x-translation transform))
         (dy (transform-y-translation transform))
         (sx (transform-x-scale transform))
         (sy (transform-y-scale transform))
         (new-transform (make-scaling-transform sx sy))
         (pw (gp:port-width plot))
         (ph (gp:port-height plot))
         )
    (declare (ignore new-transform
                     pw ph
                     dx dy))

    
    ;; (gp:clear-graphics-port plot-area)
    ;; (gp:clear-graphics-port plot)


    (with-geometry drawing-area

      (assert (and %x% %y% %width% %height%))

      (let* ((w %width%)
             (h %height%)
             (plot-frame-w (* w 0.8))
             (plot-frame-h (* h 0.8))
             (plot-frame-x (/ (- w plot-frame-w) 2.0))
             (plot-frame-y (/ (- h plot-frame-h) 2.0))

             (margin 10) ; Make this a parameter.
             (psw (gp:port-string-width drawing-area "-00000.00"))
             (psh (gp:port-string-height drawing-area "-00000.00"))

             (plot-area-w (- plot-frame-w (* 2 (+ margin psw))))
             (plot-area-h (- plot-frame-h (* 2 (+ margin psh))))

             ;; (plot-area-x (+ plot-frame-x margin psw))
             ;; (plot-area-y (+ plot-frame-y margin psh))
             (plot-area-x (+ margin psw))
             (plot-area-y (+ margin psh))

             (sw (/ w plot-frame-w))
             (sh (/ h plot-frame-h))
             )

        (declare (ignore plot-area-x plot-area-y
                         sw sh))

        (set-geometric-hint plot :x plot-frame-x)
        (set-geometric-hint plot :y plot-frame-y)
        (set-geometric-hint plot :visible-min-width plot-frame-w)
        (set-geometric-hint plot :visible-max-width plot-frame-w)
        (set-geometric-hint plot :visible-min-height plot-frame-h)
        (set-geometric-hint plot :visible-max-height plot-frame-h)

        ;; (set-geometric-hint plot-area :x x)
        ;; (set-geometric-hint plot-area :y y)
        (set-geometric-hint plot-area :visible-min-width plot-area-w)
        (set-geometric-hint plot-area :visible-max-width plot-area-w)
        (set-geometric-hint plot-area :visible-min-height plot-area-h)
        (set-geometric-hint plot-area :visible-max-height plot-area-h)

        (set-geometric-hint x-axis :visible-min-width plot-area-w)
        (set-geometric-hint x-axis :visible-max-width plot-area-w)

        (set-geometric-hint y-axis :visible-min-height plot-area-h)
        (set-geometric-hint y-axis :visible-max-height plot-area-h)

        ;; We need to fix the transform.
        ;; Essentially we need to fix the translation.
        ;;
        ;; No we don't in the new scheme, expecially for the plot frame.

        ;; (gp:apply-translation new-transform (* dx sw) (* dy sh))
        ;; (setf (plot-frame-transform plot) new-transform)


        ;; We clean now.
        ;; We need to do it here because of how Windows handles the clearing.
        ;; Note also that gp:invalidate-rectangle-seems to work,
        ;; unlike gp:clear-graphics-port.
        
        (gp:invalidate-rectangle plot-area nil nil plot-area-w plot-area-h)
        (gp:invalidate-rectangle x-axis nil nil plot-area-w nil)
        (gp:invalidate-rectangle y-axis nil nil nil plot-area-h)

        ;; (gp:clear-graphics-port plot-area)
        ;; (gp:clear-graphics-port plot)

        )))
  )


;;; erase-plot-device

(defmethod erase-plot-device ((drawing-area capi-device))
  (gp:invalidate-rectangle drawing-area))


(defmethod erase-plot-device :before ((drawing-area capi-device))
  (log-message "erase-plot-device (capi-device) ~s~%" drawing-area))


;;; erase-plot-device-object

(defmethod erase-plot-device-object ((drawing-area capi-device)
                                     (plot plot-object)
                                     x y w h)
  (declare (ignore x y w h))

  (with-geometry drawing-area
    (gp:draw-rectangle drawing-area
                       %x% %y% %width% %height%
                       :foreground (simple-pane-background drawing-area)
                       :filled t)
    ))


(defmethod erase-plot-device-object :before ((drawing-area capi-device)
                                             (plot plot-object)
                                             x y w h)
  (log-message "erase-plot-device-object (capi-device plot-object) ~s ~s~%"
               drawing-area
               plot)

  (with-geometry drawing-area
    (log-message "erase-plot-device-object (capi-device plot-object) ~s geom: ~s (~s)~%"
                 (list x y w h)
                 (list %x% %y% %width% %height%)
                 (simple-pane-background drawing-area))))
  


;;; Draw methods
;;; ------------

(defgeneric draw-graph (device device-object graph &key &allow-other-keys))
(defgeneric draw-dataset (plot-frame dataset &key &allow-other-keys))
(defgeneric draw-graph-title (plot-frame plot-object))

(defmethod name ((g graph)) (graph-name g))
(defmethod name ((d dataset)) (dataset-name d))

(defparameter *break-drawing* t)


;;; draw-pinboard-object methods.
;;; -----------------------------
;;;
;;; I need DRAW-PINBOARD-OBJECT (and REDRAW-) methods for each
;;; PINBOARD-LAYOUT object. That is, for PLOT-OBJECT and PLOT-AXIS.

(defmethod draw-pinboard-object ((drawing-area plot-area)
                                 (plot plot-object)
                                 &key x y width height
                                 &aux
                                 (margin 10)
                                 (psw (gp:port-string-width drawing-area "-00000.00"))
                                 (psh (gp:port-string-height drawing-area "-00000.00"))
                                 (czf (current-zoom-factor (element-parent drawing-area)))
                                 )
  (declare (ignorable #|x y width height|# psw psh margin))

  ;; The new version does not need anything but the scale.
  ;; Will do 3D later.

  (with-atomic-redisplay (drawing-area)
    (with-geometry drawing-area
      (log-message "draw-pinboard-object in with-atomic-redisplay ~s ~s.~%"
                   (list x y width height)
                   (list %x% %y% %width% %height%)
                   )

      ;; (gp:clear-graphics-port drawing-area)


      ;; (draw-border drawing-area :x x :y y :width width :height height :color :blue)
      ;; (draw-border drawing-area %x% %y% %width% %height% :color :red)
      ;; (draw-border drawing-area 0 0 %width% %height% :color :red)

      (gp:draw-string drawing-area
                      (format nil "D-P-O PLOT-AREA [~S] blue"
                              (list %x% %y% %width% %height% czf))
                      10
                      10
                      :foreground :blue)
      (gp:draw-string drawing-area
                      (format nil "D-P-O PLOT-OBJECT bounds [~S] red"
                              (list x y width height czf))
                      10
                      (+ 10 psh 5)
                      :foreground :red)

      (draw-border drawing-area :x 0 :y 0 :color :black)

      (gp:with-graphics-scale
          (drawing-area czf czf)

        ;; (draw-border drawing-area (+ 10 x) (+ 10 y) (+ 100 width) (+ 100 height) :color :blue)

        ;; (draw-border drawing-area 5 5 (- %width% 50) (- %height% 50) :color :green :thickness 5)

        ;; (draw-border drawing-area :x x :y y :width width :color :green :thickness 5)

        #+dont
        (gp:with-graphics-scale
            (drawing-area (/ 1 czf) (/ 1 czf))
          (gp:draw-rectangle drawing-area
                             5
                             5
                             (- %width% 10)
                             (- %height% 10)
                             :foreground :green)
          (gp:draw-rectangle drawing-area
                             10
                             10
                             (- %width% 50)
                             (- %height% 50)
                             :foreground :cyan)
          )

        ;; draw-graph (not anymore; just draw-dataset)

        (with-slots (data)
            plot
          (draw-unit-grid drawing-area)
          (when data
            (draw-dataset drawing-area data)
            ))
        ))))


;;; The :around method is used to ensure the correct zoom ratio.

(defmethod draw-pinboard-object :around ((drawing-area plot-frame)
                                         (plot plot-element)
                                         &key x y width height
                                         )
  (declare (ignorable x y width height))

  (with-geometry drawing-area
    (gp:draw-line drawing-area
                  0 0
                  0 50
                  :foreground :blue
                  :thinkness 5)
    )

  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))

      ;; (gp:with-graphics-scale (drawing-area %width% %height%)

      (gp:draw-line drawing-area
                    0 0
                    100 100
                    :foreground :grey
                    :thickness 5)

      (call-next-method))
    ;; )
    ))


(defmethod draw-pinboard-object :before ((drawing-area plot-area)
                                         (plot plot-object) 
                                         &key x y width height)
  (declare (ignore x y width height))
  (log-message "draw-pinboard-object (plot-frame plot-object) ~s ~s~%"
               drawing-area plot))


;;; Axes are drawn on the PLOT-FRAME

(defmethod draw-pinboard-object ((plot-frame plot-frame)
                                 (plot-x plot-axis-x)
                                 &key x y width height
                                 &aux
                                 (margin 10)

                                 (psw
                                  (gp:port-string-width plot-frame
                                                        "-00000.00"))

                                 (psh
                                  (gp:port-string-height plot-frame
                                                         "-00000.00"))

                                 (plot-frame-transf
                                  (plot-frame-transform plot-frame))
                                 )
  "Drawing the X axis object of a 2D plot on a PLOT-FRAME.

The drawing is done by translating 'right' and by scaling by the
actual 'width' of the plot."

  (declare (ignore x y width height))

  (with-slots (coord
               (g data)
               plot-object
               draw-labels-p
               plot-labels ; x-labels
               tick-size
               n-ticks)
      plot-x

    (with-geometry plot-frame
      (let* ((plot-width (- %width% (* 2 (+ margin psw))))
             (gmax (graph-x-max g))
             (gmin (graph-x-min g))
             (x-range (abs (- gmax gmin)))
             (zero-x (+ gmin
                        (* (nth-value 0 (gp:untransform-point
                                         plot-frame-transf
                                         0 0
                                         ))
                           x-range)))
             (max-x (+ gmin
                       (* (nth-value 0 (gp:untransform-point
                                        plot-frame-transf
                                        %width% 0
                                        ))
                          x-range)))
             (tick-labels
              (and draw-labels-p
                   (tick-labels-strings plot-labels n-ticks gmin gmax)))
             )

        (gp:with-graphics-translation
            (plot-frame (+ psw margin) 0)

          (gp:with-graphics-scale
              (plot-frame (/ plot-width %width%) 1.0)

            ;; Draw X axis labels.
            ;; (Lots of duplicated code here.  Come back and fix it.
            
            #|
            (gp:draw-string plot-frame
                            (format nil
                                    "D-P-O ~S (gmin gmax) = (~S) (zero-x max-x) = (~S) ~S"
                                    plot-frame-transf
                                    (list gmin gmax)
                                    (list zero-x max-x)
                                    x-range
                                    )
                            10 (+ 5 psh)
                            )
            |#
            (format t
                    "D-P-O X-AXIS ~S (gmin gmax) = (~S) (zero-x max-x) = (~S) ~S"
                    plot-frame-transf
                    (list gmin gmax)
                    (list zero-x max-x)
                    x-range
                    )
            
            ;; Very hairy code below.  We cannot print out all the
            ;; labels if they overlap.

            (let ((dx (/ %width% n-ticks)))
              
              (when (< plot-width
                       (loop for tick-label in tick-labels
                               sum (gp:port-string-width plot-frame (string tick-label))))
                (format t "D-P-O X-AXIS plot width ~S to small for labels.~%" plot-width)
                )

              (loop for tick-label in tick-labels
                    for i from 0
                    for tick-x = (* i dx)
                    for psw = (gp:port-string-width plot-frame (string tick-label))
                    do (gp:draw-string plot-frame
                                       tick-label
                                       (- tick-x (/ psw 2.0))
                                       (- %height% psh)
                                       ))
            
              (loop for i from 0 below (length tick-labels)
                    for tick-x = (* i dx)
                    do (gp:draw-line plot-frame
                                     tick-x (- %height% psh margin)
                                     tick-x (- %height% psh margin tick-size)
                                     :foreground :grey
                                     ))
              ))) ; gp:with-graphics-translation
        ))
    ))


(defmethod draw-pinboard-object ((plot-frame plot-frame)
                                 (plot-y plot-axis-y)
                                 &key x y width height
                                 &aux
                                 (margin 10)
                                 (psw (gp:port-string-width plot-frame "-00000.00"))
                                 (psh (gp:port-string-height plot-frame "-00000.00"))
                                 )
  "Drawing the Y axis object of a 2D plot on a PLOT-FRAME.

The drawing is done by translating 'down' and by scaling by the
actual 'height' of the plot."

  (declare (ignore x y width height))

  (with-slots (coord
               (g data)
               plot-object
               draw-labels-p
               plot-labels ; y-labels
               tick-size
               n-ticks)
      plot-y

    (with-geometry plot-frame
      (let* ((plot-height (- %height% (* 2 (+ margin psh))))
             (gmin (ffloor (graph-y-min g)))
             (gmax (fceiling (graph-y-max g)))
            
             (tick-labels
              (and draw-labels-p
                   (tick-labels-strings plot-labels n-ticks gmin gmax)))
             )
        (gp:with-graphics-translation
            (plot-frame margin (+ margin psh))
          (gp:with-graphics-scale
              (plot-frame 1.0 (/ plot-height %height%))

            ;; Draw Y axis labels.

            (let ((dy (/ %height% n-ticks)))
              (loop for tick-label in (reverse tick-labels) ; Y labels must be reversed.
                    for i from 0
                    for tick-y = (* i dy)
                    do (gp:draw-string plot-frame
                                       tick-label
                                       ; margin
                                       0
                                       tick-y
                                       ))
            
              (loop for i from 0 below (length tick-labels)
                    for tick-y = (* i dy)
                    do (gp:draw-line plot-frame
                                     psw tick-y
                                     (+ psw tick-size) tick-y
                                     ;; :thickness 20
                                     :foreground :grey
                                     ))
              
              ))) ; gp:with-graphics-translation
        ))
    ))


(defmethod draw-pinboard-object :before ((drawing-area plot-frame)
                                         (plot plot-axis)
                                         &key x y width height)
  (log-message "draw-pinboard-object (plot-frame plot-axis) ~s ~s ~s ~s ~s ~s ~s~%"
               drawing-area plot (plot-axis-coord plot) x y width height))


(defun tick-labels-strings (plot-labels n-ticks min max)
  (if plot-labels
      plot-labels
      (loop with delta = (/ (abs (- max min)) n-ticks)
            for i from min upto max by delta
            collect (format nil "~9,2f" i))))


;;;---------------------------------------------------------------------------
;;; draw-graph
;;;
;;; 20220426 MA: In the old, no pinboard objects implementation, these
;;; methods were needed to actually draw on the "capi device".  This
;;; is not needed now, as we draw directly on the PLOT-FRAME, which is
;;; a PINBOARD-LAYOUT.

;;; This method is probably all we need.  However, we maintain the
;;; other ones for backward compatibility.

(defmethod draw-graph ((drawing-area plot-frame)
                       (plot plot-object)
                       (d dataset)
                       &key
                       )
  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))
      (gp:with-graphics-scale (drawing-area %width% %height%)
        (draw-dataset drawing-area d)))))


(defmethod draw-graph ((drawing-area plot-frame)
                       (plot plot-object)
                       (d dataset-2d)
                       &key
                       )
  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))
      (gp:with-graphics-scale (drawing-area %width% %height%)
        (draw-dataset drawing-area d)))))


(defmethod draw-graph :before ((drawing-area plot-frame)
                               (plot plot-object)
                               (d dataset)
                               &key)
  (assert (eq d (plot-object-data plot))))


(defmethod draw-graph ((drawing-area plot-frame)
                       (plot plot-object)
                       (g graph-2d)
                       &key
                       )
  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))
      (gp:with-graphics-scale (drawing-area %width% %height%)
        (draw-dataset drawing-area g)))))


(defmethod draw-graph :before ((drawing-area plot-frame)
                               (plot plot-object)
                               (d graph)
                               &key)
  (assert (eq d (plot-object-data plot))))


;;;;---------------------------------------------------------------------------
;;;; draw-dataset

;;;; 20220426 MA:
;;;; 20221207 MA:
;;;; These methods are now called directly by the DRAW-PINBOARD-OBJECT
;;;; methods on PLOT-FRAME or PLOT-AREA and PLOT-OBJECT.
;;;;
;;:; Datasets are are drawn on PLOT-AREAs.

(defmethod draw-dataset ((drawing-area plot-area) (d dataset-2d)
                         &key
                         &allow-other-keys)
  (let* ((dsnp (dataset-normalized-points d))
         (dataset-polygons
          (if (some (lambda (xy) (eq :missing-value (second xy))) dsnp)
              (split-sequence:split-sequence-if
               (lambda (xy) (eq :missing-value (second xy)))
               dsnp)
              (list dsnp)))
         )

    ;; (format t "dataset-2d ~S~%" dsnp)
    ;; (format t "dataset-2d ~S~%" dataset-polygons)

    (dolist (dsnp dataset-polygons)
      (gp:draw-polygon drawing-area
                       dsnp
                       ;; :filled nil
                       :closed nil
                       :foreground (choose-color)))))


;;; Test placeholder

(defun point-2d-outsite-view-p (x y min-x min-y width height)
  (cond ((< x min-x) t)
        ((< (+ min-x width) x) t)
        ((< y min-y) t)
        ((< (+ min-y height) y) t)
        (t nil)))


(defun normalize-pt (pt pt-min pt-max)
  (declare (type real pt pt-min pt-max))
  (/ (+ pt pt-min) pt-max)
  )

(declaim (inline normalize-pt))

#+nil
(defun normalize-pt-unit-interval (pt pt-min pt-max)
  (declare (type real pt pt-min pt-max))
  (/ (+ pt pt-min) pt-max)
  )


(defun flatten-pair-list (pair-list)
  (declare (type list pair-list))
  (loop for (x y) in pair-list
        collect x
        collect y))

(declaim (ftype (function (list) list) flatten-pair-list)
         (inline flatten-pair-list))


(defun mirror-y-coord (coord-list)
  (declare (type list coord-list))
  (loop for (x y) on coord-list by #'cddr
        collect x
        collect (- y)))

(declaim (ftype (function (list) list) mirror-y-coord)
         (inline mirror-y-coord))


(defun split-polygon (ds-polygon)
  (declare (type sequence ds-polygon))
  (if (find :missing-value ds-polygon :test #'eq :key #'second)
      (split-sequence:split-sequence :missing-value
                                     ds-polygon
                                     :test #'eq
                                     :key #'second)
      (list ds-polygon))
  )

(declaim (ftype (function (sequence) list) split-polygon)
         (inline split-polygon))


(defmethod draw-dataset ((drawing-area plot-area)
                         (g graph-2d)
                         &key
                         &allow-other-keys)

  ;; Datasets are always drawn with the correct "current zoom factor".

  (with-geometry drawing-area

    (format t "T1 ~S~%" (gp:graphics-port-transform drawing-area))

    (let* ((gxmin (graph-x-min g))
           (gymin (graph-y-min g))
           (gxmax (graph-x-max g))
           (gymax (graph-y-max g))

           (abs-min-x (abs gxmin))
           (abs-min-y (abs gymin))
           (abs-max-x (abs gxmax))
           (abs-max-y (abs gymax))

           (new-graph-max-x (+ abs-max-x abs-min-x))
           (new-graph-max-y (+ abs-max-y abs-min-y))
           )

      (declare (type real
                     gxmin gymin gxmax gymax
                     abs-max-x abs-min-y abs-max-x abs-max-y
                     new-graph-max-x new-graph-max-y))

      (gp:draw-string drawing-area
                      (format nil
                              "Graph box ~S => ~S"
                              (list (graph-x-min g)
                                    (graph-y-min g)
                                    (graph-x-max g)
                                    (graph-y-max g))
                              (list new-graph-max-x new-graph-max-y))
                      50 50)

      (gp:with-graphics-scale ; Scale for actual size of drawing.
          (drawing-area (/ %width% 2.0) (/ %height% 2.0))

        (gp:with-graphics-translation
            (drawing-area 1 1) ; The data points are within (-1 -1) and (1 1).

          (format t "T2 ~S~%" (gp:graphics-port-transform drawing-area))

          (gp:draw-line drawing-area 0 0 0.5 0.5
                        :foreground :blue
                        :thickness (/ 1.0 (max %width% %height%)))

          (gp:draw-line drawing-area -1 -1 -0.4 -0.4
                        :foreground :blue
                        :thickness (/ 1.0 (max %width% %height%)))

          ;; First, normalize the plot coords between -1.0 and 1.0
          ;; (making sure to break the plots when there are :missing-value's).
          ;; Next draw each dataset/polygon.

          (loop for d in (graph-datasets g)
                for y from 0
                for dataset-polygon  = (dataset-normalized-points d)
                for dataset-polygons = (split-polygon dataset-polygon)
                do
                  (dolist (dps dataset-polygons)
                    (format t "Drawing poly ~S~%Transform ~S~%"
                            dps
                            (gp:graphics-port-transform drawing-area))
                  
                    (gp:draw-polygon drawing-area
                                     (mirror-y-coord (flatten-pair-list dps))
                                     :filled nil
                                     :closed nil
                                     ;; :foreground (choose-color i)
                                     :thickness (/ (max %width% %height%))
                                     )
                    )
                )
          ))
      )))


;;;---------------------------------------------------------------------------
;;; Drawing the X and Y axes.
;;;
;;; 20220409: Drawing directly does not work (anymore?) on LWM. A
;;; solution is to build a "spec" (a pinboard-object) for the axes and
;;; then add them or draw them properly according to CAPI conventions.

(defun make-plot-object-axis (plot-object data coord width height
                                          &key
                                          (tick-size 10)
                                          (n-ticks 10)
                                          (draw-labels-p t)
                                          &allow-other-keys
                                          &aux
                                          (axis-class
                                           (ecase coord
                                             (:x 'plot-axis-x)
                                             (:y 'plot-axis-y)
                                             (:z 'plot-axis-z)
                                             ))
                                          )
  (make-instance axis-class
                 :data data
                 :coord coord ; Redundant
                 :height height
                 :width width
                 :plot-object plot-object
                 :draw-labels-p draw-labels-p
                 :tick-size tick-size
                 :n-ticks n-ticks
                 ))


;;;---------------------------------------------------------------------------
;;; Legend

(defclass plot-frame-object-legend (drawn-pinboard-object)
  ((legend-items :accessor legend-items :initarg :legend-items)))


(defparameter *legend-horizontal-margin* 4)
(defparameter *legend-vertical-margin* 4)


(defmethod compute-legend-size ((drawing-area plot-frame) (d dataset))
  (values (* 2 *legend-horizontal-margin*)
          (* 2 *legend-vertical-margin*))
  )


(defmethod compute-legend-size ((drawing-area plot-frame) (d graph))
  (let* ((ds (graph-datasets d))
         (psh (gp:port-string-height drawing-area "FOO"))
         (max-label-width
          (loop for d in ds
                maximize (gp:port-string-width drawing-area (string (name d)))))
         (max-labels-height (* (+ psh *legend-vertical-margin*) (length ds)))
         (legend-width (+ max-label-width (* *legend-horizontal-margin* 2)))
         (legend-height (+ max-labels-height (* *legend-vertical-margin* 2)))
         )
    (values legend-width
            legend-height)
    ))


(defmethod draw-legend ((drawing-area plot-frame)
                        (plot plot-object)
                        (d dataset)
                        &key)

  (let ((x 1.2)
        (y 0)
        )
    (gp:draw-string drawing-area (string (dataset-name d)) x y
                    :foreground (choose-color))
    ))


(defmethod draw-legend ((drawing-area plot-frame)
                        (plot plot-object)
                        (g graph)
                        &key 
                        )
  (with-geometry plot
    (let* ((ds (graph-datasets g))
           (psh (gp:port-string-height drawing-area "FOO"))
           (max-label-width
            (loop for d in ds
                  maximize (gp:port-string-width drawing-area (string (name d)))))
           (max-labels-height (* (+ psh *legend-vertical-margin*) (length ds)))
           (legend-width (+ max-label-width (* *legend-horizontal-margin* 2)))
           (legend-height (+ max-labels-height (* *legend-vertical-margin* 2)))
           (legend-x (- %width% (+ legend-width *legend-horizontal-margin*)))
           (legend-y *legend-vertical-margin*)
           )

      (gp:draw-rectangle drawing-area
                         legend-x legend-y
                         legend-width legend-height
                         :foreground :white
                         :filled t)
      (gp:draw-rectangle drawing-area
                         legend-x legend-y
                         legend-width legend-height)

      (loop with x = (+ legend-x *legend-horizontal-margin*)
            for d in ds
            for y from (+ legend-y *legend-horizontal-margin* psh)
              by (+ *legend-horizontal-margin* psh)
            for i from 0
            do (gp:draw-string drawing-area (string (name d))
                               x y
                               :foreground (choose-color i))
            ))
      ))


;;; (defgeneric add-plot (new-plot-designator &optional current-plot-output &key &allow-other-keys))

;;; (defgeneric remove-plot (plot-designator &optional current-plot-output &key &allow-other-keys))

;;; (defgeneric clear (&optional current-plot-output))


;;;---------------------------------------------------------------------------
;;; Interface functions.

(defun resize-capi-device (capi-device x y w h)
  (erase-plot-device capi-device)
  (dolist (plot-frame (layout-description capi-device))
    (resize-plot-device-object capi-device plot-frame x y w h)
    )
  ;; (erase-plot-device capi-device)
  )


;;;---------------------------------------------------------------------------
;;; Utility

(defun extreme (seq &key (test #'<) (start 0) end)
  (loop with lseq = (length seq)
        with max = (elt seq start)
        with end = (if end (min end lseq) lseq)
        for i from start below end
        for e =  (elt seq i)
        when (funcall test max e) do (setf max e)
        finally (return max)))

;;;; end of file -- plot-protocol-impl-capi.lisp --
