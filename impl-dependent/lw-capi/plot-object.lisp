;;; -*- Mode: Lisp -*-

;;;; plot-object.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)


;;;---------------------------------------------------------------------------
;;; Definitions.


;;; plot-object --

(defclass plot-object (plot-element) ; No need to be a DRAWN-PINBOARD-OBJECT.
  ((data :accessor plot-object-data
         :initarg :data
         :initform nil
         :type (or null graph dataset))
   (draw-legend-p :accessor draw-legend-p :initarg :draw-legend-p)
   (draw-x-labels-p :accessor draw-x-labels-p
                  :initarg :draw-x-labels-p)
   (draw-y-labels-p :accessor draw-y-labels-p
                    :initarg :draw-y-labels-p)
   (tick-size :accessor tick-size
              :initarg :tick-size)
   )
  (:default-initargs
   :draw-legend-p t
   :draw-x-labels-p t
   :draw-y-labels-p t
   :tick-size 10
   )
  (:documentation "The Plot Object Class.")
  )


(defgeneric plot-object-p (x)
  (:method ((x plot-object)) t)
  (:method ((x t)) nil))



;;;---------------------------------------------------------------------------
;;; Implementation (to be put elsewhere)

(defun plot-frame-device (plot-frame)
  (declare (type plot-frame plot-frame))
  (element-parent plot-frame))

(defun plot-object-frame (plot-object)
  (declare (type plot-object plot-object))
  (element-parent plot-object))

(defun plot-object-device (plot-object)
  (declare (type plot-object plot-object))
  (element-parent (element-parent plot-object)))

(defun capi-device-plot-frame (capi-device)
  (declare (type capi-device capi-device))
  (find-if #'plot-frame-p (layout-description capi-device)))

;;; end of file -- plot-object.lisp --
