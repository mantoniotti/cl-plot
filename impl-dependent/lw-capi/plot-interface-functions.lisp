;;; -*- Mode: Lisp -*-

;;;; plot-interface-functions.lisp
;;;;
;;;; Plotting utilities for LW; functions for LW interface.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT-CAPI")

(defun plot-interface-zoom-in (interface)
  (declare (type plot-interface interface))
  (let ((plot-frame (capi-device-plot-frame (plot-interface-device interface))))
    (declare (type (or null plot-frame) plot-frame))
    (when plot-frame
      (setf (current-operation plot-frame)
            :zoom

            (simple-pane-cursor plot-frame)
            :crosshair

            (current-zoom-factor plot-frame)
            (zoom-in-factor plot-frame)))))


(defun plot-interface-zoom-out (interface)
  (declare (type plot-interface interface))
  (let ((plot-frame (capi-device-plot-frame (plot-interface-device interface))))
    (declare (type (or null plot-frame) plot-frame))
    (when plot-frame
      (setf (current-operation plot-frame)
            :zoom
            
            (simple-pane-cursor plot-frame)
            :crosshair

            (current-zoom-factor plot-frame)
            (zoom-out-factor plot-frame)))))

;;; end of file -- plot-interface-functions.lisp --
