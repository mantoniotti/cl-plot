;;; -*- Mode: Lisp -*-

;;;; plot-area.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)


;;;---------------------------------------------------------------------------
;;; Definitions.


;;; plot-area --

(defclass plot-area (simple-pinboard-layout)
  ()
  (:documentation "The CL-PLOT CAPI Plot Area Class.")
  )

(defgeneric plot-area-p (x)
  (:method ((x plot-area)) t)
  (:method ((x t)) nil))


;;; end of file -- plot-area.lisp --
