;;; -*- Mode: Lisp -*-

;;;; logging.lisp --
;;;; Log and debug utilities fot the LW CAPI implementation.
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.

(in-package "CL-PLOT-CAPI")


(defparameter *cl-plot-config-folder*
  (clad:ensure-app-or-library-data-folder "cl-plot-capi"))


(defun make-plot-config-log-pathname ()
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time (get-universal-time))
    (declare (ignore second))
    (let ((log-filename
           (format nil "cl-plot-~D~2,'0D~2,'0D-~2,'0D~2,'0D"
                   year month date hour minute))
          )
      (make-pathname :name log-filename
                     :type "log"
                     :defaults *cl-plot-config-folder*))))

(defparameter *cl-plot-config-log* (make-plot-config-log-pathname))


(defun reset-plot-config-log-pathname ()
  (setf *cl-plot-config-log* (make-plot-config-log-pathname)))


(defparameter *cl-plot-log-p* nil)

(defun log-message (fmt &rest args)
  (when *cl-plot-log-p*
    (with-open-file (logfile *cl-plot-config-log*
                             :direction :output
                             :if-exists :append
                             :if-does-not-exist :create)
      (apply #'format
             logfile
             ";;; CL-PLOT: ~@?"
             fmt
             args))))
    

;;;; end of file -- logging.lisp --
