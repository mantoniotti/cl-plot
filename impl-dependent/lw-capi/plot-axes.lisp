;;; -*- Mode: Lisp -*-

;;;; plot-axes.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)


;;;---------------------------------------------------------------------------
;;; Definitions.


;;; cartesian-axes --

(deftype cartesian-axes ()
  '(member :x :y :z))


;;; axis-kind --

(deftype axis-kind ()
  '(member integer real symbol) ; Where 'symbol' means "categorical".
  )


;;; plot-axis --

(defclass plot-axis (plot-element)
   ((coord :accessor plot-axis-coord
           :initarg :coordinate
           :initarg :coord
           :type cartesian-axes)

    (data :accessor plot-axis-data
          :initarg :data
          :type (or null list vector graph dataset)
          )

    (plot-object :accessor plot-axis-plot-object
                 :initarg :plot-object
                 ;; The plot object this axis "belongs to".
                 )

    ;; The DATA slot is EQUAL to the DATA slot of the object in slot
    ;; PLOT-OBJECT.

    ;; Appearance and styling slots.

    (axis-kind :reader plot-axis-kind
               :initform 'real
               :initarg :kind
               :type axis-kind)

    (draw-labels-p :accessor plot-axis-draw-labels-p
                   :initarg :draw-x-labels-p
                   :initform t)

    (tick-size :accessor plot-axis-tick-size
               :initarg :tick-size
               :initform 10)

    ;; Computed slots.

    (plot-labels :reader plot-axis-labels
                 :writer set-plot-axis-labels
                 :initarg :labels
                 )

    (n-ticks :accessor plot-axis-n-ticks
             :initarg :n-ticks
             :initform 10
             )
    )
   )


(defclass plot-axis-x (plot-axis)
  ((coord :initform :x)))


(defclass plot-axis-y (plot-axis)
  ((coord :initform :y)))


(defclass plot-axis-z (plot-axis)
  ((coord :initform :z)))


(defgeneric plot-axis-p (a)
  (:method ((a plot-axis)) t)
  (:method ((a t)) nil)
  )


(defmethod initialize-instance :after ((pa plot-axis)
                                       &key
                                       &allow-other-keys)

  (assert (slot-boundp pa 'coord)
      ()
    "CL-PLOT.CAPI: slot COORD is unbound in ~S." pa)

  (assert (slot-boundp pa 'data)
      ()
    "CL-PLOT.CAPI: slot DATA is unbound in ~S." pa)


  (unless (slot-boundp pa 'plot-labels)
    (with-slots (data)
        pa
      (let ((plot-labels (case (plot-axis-coord pa)
                           (:x (x-labels data))
                           (:y (y-labels data))
                           (:z (z-labels data))))
            )
        (set-plot-axis-labels plot-labels pa))))

  (when (plot-axis-labels pa)
    (setf (plot-axis-n-ticks pa) (length (plot-axis-labels pa))))
  )



(defmethod x-labels ((l list))
  (loop for i from 0 below (length l) collect i))

(defmethod y-labels ((l list))
  (loop for i from 0 below (length l) collect i))

(defmethod z-labels ((l list))
  (loop for i from 0 below (length l) collect i))


(defmethod x-labels ((l vector))
  (loop for i from 0 below (length l) collect i))

(defmethod y-labels ((l vector))
  (loop for i from 0 below (length l) collect i))

(defmethod z-labels ((l vector))
  (loop for i from 0 below (length l) collect i))
  


;;; end of file -- plot-axes.lisp --
