;;;; -*- Mode: Lisp -*-

;;;; cl-plot-capi-pkg.lisp
;;;;
;;;; Plotting utilities for LW; CAPI package definition.
;;;;
;;;; See the file COPYING for copyright and licensing information.



(defpackage "IT.UNIMIB.DISCO.CL-PLOT.CAPI" (:use "COMMON-LISP" "CL-PLOT" "CAPI")
  (:nicknames "CL-PLOT.CAPI" "CL-PLOT-CAPI")
  (:export
   "PLOT-INTERFACE"
   "CAPI-DEVICE")

  (:import-from "CL-PLOT" "DATASET-RAW-DATA")
  )

;;;; end of file -- cl-plot-capi-pkg.lisp --
