;;;; -*- Mode: Lisp -*-

;;;; plot-setup.lisp


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)


(defmethod plot-setup ((interface plot-interface)
                       &key
                       &allow-other-keys)
  (execute-with-interface interface
                          #'plot-setup
                          (plot-interface-device interface)))


(defmethod plot-setup ((drawing-area capi-device)
                       &key
                       ((:graph-2d g))

                       (draw-legend-p t)
                       (draw-x-labels-p t)
                       (draw-y-labels-p t)
                       (tick-size 10)
                       (margin 10)
                       &allow-other-keys)

  (multiple-value-bind (min-width min-height)
      (get-constraints drawing-area) ; (values 10 10)

    (assert (and min-width min-height) (min-width min-height))

    (with-geometry drawing-area
      (let* ((w (or %width% min-width))
             (h (or %height% min-height))
             (plot-frame-w (round (* w 0.8)))
             (plot-frame-h (round (* h 0.8)))
             (plot-frame-x (round (/ (- w plot-frame-w) 2.0)))
             (plot-frame-y (round (/ (- h plot-frame-h) 2.0)))

             (psw (gp:port-string-width drawing-area "-00000.00"))
             (psh (gp:port-string-height drawing-area "-00000.00"))

             (plot-area-w (- plot-frame-w (* 2 (+ margin psw))))
             (plot-area-h (- plot-frame-h (* 2 (+ margin psh))))
             (plot-area-x (+ margin psw))
             (plot-area-y (+ margin psh))

             (new-plot-frame
              (make-instance 'plot-frame
                             :x plot-frame-x
                             :y plot-frame-y
                             :width plot-frame-w :height plot-frame-h

                             :visible-min-width plot-frame-w
                             :visible-min-height plot-frame-h
                             :visible-max-width plot-frame-w
                             :visible-max-height plot-frame-h

                             :draw-with-buffer t ; Windows may need it.

                             :background :white
                             :input-model '(((:button-1 :press) press-button)
                                            ((:button-1 :release) release-button)
                                            )
                             ))

             (new-plot-area
              (make-instance 'plot-area
                             :x plot-area-x
                             :y plot-area-y
                             :background :white
                             :width  plot-area-w
                             :height plot-area-h
                             :internal-min-width  plot-area-w
                             :internal-min-height plot-area-h
                             :internal-max-width  plot-area-w
                             :internal-max-height plot-area-h

                             :draw-with-buffer t ; Windows may need it.
                             ))

             
             (new-plot-object
              (make-instance 'plot-object
                             :draw-legend-p   draw-legend-p
                             :draw-x-labels-p draw-x-labels-p
                             :draw-y-labels-p draw-y-labels-p
                             :tick-size tick-size
                             ;; :data g
                             ))

             (plot-object-title
              (when g
                (make-plot-object-title drawing-area g
                                        plot-frame-x plot-frame-y
                                        (1- plot-frame-w) (1- plot-frame-h))))

             (x-axis-object
              (when g
                (make-plot-object-axis new-plot-object
                                       g
                                       :x
                                       plot-area-w
                                       plot-area-h
                                       :draw-labels-p draw-x-labels-p
                                       :tick-size tick-size)))

             (y-axis-object
              (when g
                (make-plot-object-axis new-plot-object
                                       g
                                       :y
                                       plot-area-w
                                       plot-area-h
                                       :draw-labels-p draw-y-labels-p
                                       :tick-size tick-size)))
             )

        (log-message "plot-data ~S ==> ~S~%"
                     (list %x% %y% %width% %height%)
                     (list plot-frame-x plot-frame-y plot-frame-w plot-frame-h))
        (log-message "plot-data plot-object ~S~%"
                     (list plot-area-x plot-area-y plot-area-w plot-area-h))
        (log-message "plot-data plot-object psw psh ~S~%"
                     (list psw psh))

        ;; Build the hierarchy.

        (setf (layout-description drawing-area) (list new-plot-frame))

        (setf (layout-description new-plot-frame) (list new-plot-area))

        (when g
          (manipulate-pinboard new-plot-frame x-axis-object :add-top)
          (manipulate-pinboard new-plot-frame y-axis-object :add-top))

        (manipulate-pinboard new-plot-area new-plot-object :add-top)

        (when g
          (manipulate-pinboard drawing-area plot-object-title :add-top)) 

        ;; Return the (modified) CAPI-DEVICE.
        drawing-area
        ))))

;;;; end of file -- plot-setup.lisp --
