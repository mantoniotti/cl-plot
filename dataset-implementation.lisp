;;;; -*- Mode: Lisp -*-

;;;; dataset-implementation.lisp
;;;
;;;; Plotting utilities for LW; "dataset" implementations.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")


;;;---------------------------------------------------------------------------
;;; Constructors and initializers.

(defgeneric destructure-dataset (data))

(defgeneric make-dataset* (data &key name &allow-other-keys))


(defmethod make-dataset* ((data list)
                          &key (name (gensym "DATASET-"))
                          &allow-other-keys) 
  (destructuring-bind (x-data y-data &optional z-data)
      data
    (let ((kind (if z-data :3d :2d)))
      (case kind
        (:2d
         (assert (= (length x-data) (length y-data)))
         (make-instance 'dataset-2d
                        :name name
                        :x-data x-data
                        :y-data y-data))
        (:3d
         (assert (= (length x-data) (length y-data) (length z-data)))
         (make-instance 'dataset-3d
                        :name name
                        :x-data x-data
                        :y-data y-data
                        :z-data z-data)))
      )))


(defmethod make-dataset* ((data vector)
                          &key (name (gensym "DATASET-"))
                          &allow-other-keys)
  (case (length data)
    (1 (make-instance 'dataset-2d
                      :name name
                      :x-data (range 0 (length (aref data 0)))
                      :y-data (aref data 0)))
    (2 (make-instance 'dataset-2d
                      :name name
                      :x-data (aref data 0)
                      :y-data (aref data 1)))
    (3 (make-instance 'dataset-3d
                      :name name
                      :x-data (aref data 0)
                      :y-data (aref data 1)
                      :z-data (aref data 2)))
    (t (make-instance 'dataset-2d
                      :name name
                      :x-data (range 0 (length data))
                      :y-data data))
    ))


(defmethod make-dataset* ((data array)
                          &key (name (gensym "DATASET-"))
                          &allow-other-keys)
  (assert (< 0 (array-rank data) 4))
  (multiple-value-bind (x-data y-data z-data)
      (destructure-dataset data)
    (let ((kind (if z-data :3d :2d)))
      (case kind
        (:2d
         (assert (= (length x-data) (length y-data)))
         (make-instance 'dataset-2d
                        :name name
                        :x-data x-data
                        :y-data y-data))
        (:3d
         (assert (= (length x-data) (length y-data) (length z-data)))
         (make-instance 'dataset-3d
                        :name name
                        :x-data x-data
                        :y-data y-data
                        :z-data z-data)))
      )))


;;; make-dataset

(defun make-dataset (&key 
                     (name (gensym "DATASET-"))
                     (y-data ())
                     (x-data (range 0 (length y-data)))
                     (z-data () z-data-supplied-p))
  (let ((kind (if z-data-supplied-p :3d :2d)))
    (case kind
      (:2d
       (assert (= (length x-data) (length y-data)))
       (make-instance 'dataset-2d
                      :name name
                      :x-data x-data
                      :y-data y-data))
      (:3d
       (assert (= (length x-data) (length y-data) (length z-data)))
       (make-instance 'dataset-3d
                      :name name
                      :x-data x-data
                      :y-data y-data
                      :z-data z-data)))
    ))


(defmethod initialize-instance :after ((g dataset-2d) &key)
  (setf (dataset-x-data g)
        (coerce (dataset-x-data g) 'vector))

  (loop for x across (dataset-x-data g)
        when (numberp x) maximize x into xmax
        when (numberp x) minimize x into xmin
        finally (setf (slot-value g 'x-max) xmax
                      (slot-value g 'x-min) xmin))

  (setf (dataset-y-data g)
        (coerce (dataset-y-data g) 'vector))

  (loop for y across (dataset-y-data g)
        when (numberp y) maximize y into ymax
        when (numberp y) minimize y into ymin
        finally (setf (slot-value g 'y-max) ymax
                      (slot-value g 'y-min) ymin))

  )


(defmethod initialize-instance :after ((g dataset-3d) &key)
  (setf (dataset-z-data g)
        (coerce (dataset-z-data g) 'vector))

  (loop for z across (dataset-z-data g)
        when (numberp z) maximize z into zmax
        when (numberp z) minimize z into zmin
        finally (setf (slot-value g 'z-max) zmax
                      (slot-value g 'z-min) zmin)))


;;;---------------------------------------------------------------------------
;;; make-shaped-dataset

(defgeneric make-shaped-dataset (data shape dimensions &key &allow-other-keys)
  )


;;; dataset methods

(defmethod make-shaped-dataset ((data dataset-2d)
                                (shape (eql :line))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  data)


;;; :line methods

#+oldversion
(defmethod make-shaped-dataset ((data list)
                                (shape (eql :line))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  (typecase (first data)
    (real
     ;; Assume this is a list of numbers, i.e. a 1d dataset.
     (make-instance 'dataset-2d :y-data data))
    (complex
     (make-instance 'dataset-2d
                    :y-data (mapcar #'imagpart data)
                    :x-data (mapcar #'realpart data)))
    (list
     ;; Two cases.
     ;; 1 - two subsequences Xs and Yx
     ;; 2 - a list of pairs
     (cond ((and (= (list-length data) 2)
                 (listp (first data)) (listp (second data))
                 (realp (first (first data))) (realp (first (second data))))
            (destructuring-bind (xs ys)
                data
              (make-instance 'dataset-2d :x-data xs :y-data ys)))
           ((> (list-length data) 2)
            (loop for (x y) in data
                  collect x into xs
                  collect y into ys
                  finally
                  (return (make-instance 'dataset-2d :x-data xs :y-data ys))))
           (t
            (error "Cannot make 2D dataset from ~S." data)
            ))
     )))



(defmethod make-shaped-dataset ((data list)
                                (shape (eql :line))
                                (dimensions (eql :2d)) 
                                &key &allow-other-keys)
  (typecase (first data)
    ((or real list)
     ;; Assume this is a list of numbers, i.e. a 1d dataset.
     (multiple-value-bind (xs ys zs)
         (destructure-dataset data)
       (when zs
         (warn "2D line dataset called for, but data is 3D; Zs will be dropped."))
       (make-instance 'dataset-2d :x-data xs :y-data ys)))
    (complex
     (make-instance 'dataset-2d
                    :y-data (mapcar #'imagpart data)
                    :x-data (mapcar #'realpart data)))
    ))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :line))
                                (dimensions (eql :2d))
                                &key
                                (column-data-p t)
                                &allow-other-keys)
  (ecase (array-rank data)
    (0 (make-instance 'dataset-2d))
    (1 (typecase (array-element-type data) ; Damn defaults!
         (real (make-instance 'dataset-2d
                              :x-data (range 0 (length data))
                              :y-data (copy-seq data)))
         (complex (make-instance 'dataset-2d
                                 :y-data (map 'list #'imagpart data)
                                 :x-data (map 'list #'realpart data)))
         (t
          (typecase (aref data 0)
            (real (make-instance 'dataset-2d
                                 :x-data (range 0 (length data))
                                 :y-data (copy-seq data)))
            (complex (make-instance 'dataset-2d
                                    :y-data (map 'list #'imagpart data)
                                    :x-data (map 'list #'realpart data)))
            (t (error "Cannot plot a vector of non numbers (cfr DATA[0] = ~S)."
                      (aref data 0)))
            ))))
    (2 (if column-data-p
           (make-instance 'dataset-2d
                          :x-data (matrix-column data 0)
                          :y-data (matrix-column data 1))
           (make-instance 'dataset-2d
                          :x-data (matrix-row data 0)
                          :y-data (matrix-row data 1))
           ))
    ))


;;; :bar methods

(defun make-group-dataset (dataset-class data x-data)
  (make-instance dataset-class :raw-data data :x-data x-data))


(defmethod make-shaped-dataset ((data list)
                                (shape (eql :bar))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  ;; Assumes the list contains sublists of the following forms, no
  ;; error checking done:
  ;;
  ;;	(n1i n2i n3i ... nki)
  ;; or
  ;;	(ki (n1i n2i n3i ... nki))
  ;;
  ;; In the first case the sublist is considered to be a "group"
  ;; indexed by position.  In the second case it is considered to be a
  ;; "group" indexed by 'ki'.
  
  (let ((plain-group-p (not (listp (second (first data))))))
    (if plain-group-p
        (make-group-dataset 'bar-dataset
                            data
                            (range 0 (list-length data)))
        (make-group-dataset 'bar-dataset
                            (mapcar #'second data)
                            (mapcar #'first data))
        )))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :bar))
                                (dimensions (eql :2d)) 
                                &key
                                (header-row-p nil)
                                (x-data
                                 (data-index-vector data))
                                &allow-other-keys)

  (declare (ignore header-row-p))

  ;; Assumes the array is a 2D matrix.
  
  (assert (= (length x-data) (array-dimension data 0)))
  (make-group-dataset 'bar-dataset data x-data))


;;; :area methods

(defmethod make-shaped-dataset ((data list)
                                (shape (eql :area))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  ;; Assumes the list contains sublists of the following forms, no
  ;; error checking done:
  ;;
  ;;	(n1i n2i n3i ... nki)
  ;; or
  ;;	(ki (n1i n2i n3i ... nki))
  ;;
  ;; In the first case the sublist is considered to be a "group"
  ;; indexed by position.  In the second case it is considered to be a
  ;; "group" indexed by 'ki'.
  
  (let ((plain-group-p (not (listp (second (first data))))))
    (if plain-group-p
        (make-group-dataset 'area-dataset data (range 0 (list-length data)))
        (make-group-dataset 'area-dataset  (mapcar #'second data) (mapcar #'first data))
        )))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :area))
                                (dimensions (eql :2d))
                                &key
                                (header-row-p nil)
                                (x-data
                                 (data-index-vector data))
                                &allow-other-keys)

  (declare (ignore header-row-p))

  ;; Assumes the array is a 2D matrix.
  
  (assert (= (length x-data) (array-dimension data 0)))
  (make-group-dataset 'area-dataset data x-data))


;;; :stack methods

(defmethod make-shaped-dataset ((data list)
                                (shape (eql :stack))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  ;; Assumes the list contains sublists of the following forms, no
  ;; error checking done:
  ;;
  ;;	(n1i n2i n3i ... nki)
  ;; or
  ;;	(ki (n1i n2i n3i ... nki))
  ;;
  ;; In the first case the sublist is considered to be a "group"
  ;; indexed by position.  In the second case it is considered to be a
  ;; "group" indexed by 'ki'.
  
  (let ((plain-group-p (not (listp (second (first data))))))
    (if plain-group-p
        (make-group-dataset 'stack-dataset data (range 0 (list-length data)))
        (make-group-dataset 'stack-dataset  (mapcar #'second data) (mapcar #'first data))
        )))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :stack))
                                (dimensions (eql :2d))
                                &key
                                (header-row-p nil)
                                (x-data
                                 (data-index-vector data))
                                &allow-other-keys)

  (declare (ignore header-row-p))

  ;; Assumes the array is a 2D matrix.
  
  (assert (= (length x-data) (array-dimension data 0)))
  (make-group-dataset 'stack-dataset data x-data))


;;;---------------------------------------------------------------------------
;;; Destructuring various datasets.
;;;
;;; TODO: add new formats. Vectors of points etc etc.

(defun dotted-pair-p (x)
  (and (consp x) (cdr x) (atom (cdr x))))


(defun pairp (x)
  (or (dotted-pair-p x)
      (and (vectorp x) (= 2 (length x)))
      (and (listp x) (null (cdr (last x))) (= (list-length x) 2))))


(defun triplep (x)
  (and (typep x 'sequence) (= 3 (length x))))


(defun pair-0 (x)
  (assert (pairp x))
  (cond ((or (dotted-pair-p x)
             (and (listp x) (null (cdr (last x))) (= (list-length x) 2)))
         (car x))
        ((and (vectorp x) (= 2 (length x))) (aref x 0))))


(defun pair-1 (x)
  (assert (pairp x))
  (cond ((dotted-pair-p x) (cdr x))
        ((and (listp x) (null (cdr (last x))) (= (list-length x) 2))
         (second x))
        ((and (vectorp x) (= 2 (length x))) (aref x 1))))


(defmethod destructure-dataset ((a array))
  (case (array-rank a)
    (0 ())
    (1 (copy-seq a))
    (2 (values (loop for i from 0 below (array-dimension a 0)
                     collect (aref a i 0))
               (loop for i from 0 below (array-dimension a 1)
                     collect (aref a i 1))))
    (3 (values (loop for i from 0 below (array-dimension a 0)
                     collect (aref a i 0))
               (loop for i from 0 below (array-dimension a 1)
                     collect (aref a i 1))
               (loop for i from 0 below (array-dimension a 2)
                     collect (aref a i 2))))
    ))


(defmethod destructure-dataset ((a list))
  (cond ((every #'numberp a) 
         (values (loop for nil in a ; Check out the NIL! Cute!
                       for x from 0
                       collect x)
                 (copy-list a)))
        ((every #'pairp a)
         (values (loop for x in a
                       collect (pair-0 x))
                 (loop for x in a
                       collect (pair-1 x))))
        ((every #'triplep a)
         (values (loop for x in a collect (elt x 0))
                 (loop for x in a collect (elt x 1))
                 (loop for x in a collect (elt x 2))
                 ))
        (t
         (error "Badly shaped list dataset; only singletons, pairs and triplets accepted."))
        ))

;;;---------------------------------------------------------------------------
;;; Extremes.

;;; These are catch-all methods and are incorrect.

(defmethod dataset-x-max ((d dataset)) 0)
(defmethod dataset-y-max ((d dataset)) 0)
(defmethod dataset-z-max ((d dataset)) 0)

(defmethod dataset-x-min ((d dataset)) 0)
(defmethod dataset-y-min ((d dataset)) 0)
(defmethod dataset-z-min ((d dataset)) 0)


;;; These GRAPH-2D methods are here for completeness.

(defmethod dataset-z-max ((d dataset-2d)) 0)
(defmethod dataset-z-min ((d dataset-2d)) 0)


;;;---------------------------------------------------------------------------
;;; Computing the normalized points

(defmethod compute-normalized-points ((d dataset))
  ;; No-op.
  )


#|
We want the y on [-1, 1] corresponding to x.

1.
ymin = m xmin + d
ymax = m xmax + d

Given: y = m x + d

2.
d = ymin - m xmin
ymax = m xmax + ymin - m xmin

3.
d = ymin - m xmin
ymax = m (xmax - xmin) + ymin

4.
d = ymin - m xmin
ymax - ymin = m (xmax - xmin)

5.
d = ymin - m xmin
m = (ymax - ymin) / (xmax - xmin)

6.
d = ymin - (((ymax - ymin) / (xmax - xmin)) xmin)
m = (ymax - ymin) / (xmax - xmin)

7.
d = (1/(xmax - xmin)) (ymin (xmax - xmin) - xmin (ymax - ymin))
m = (ymax - ymin) / (xmax - xmin)

8.
d = (1/(xmax - xmin)) (ymin xmax - ymin xmin - xmin ymax + xmin ymim)
m = (ymax - ymin) / (xmax - xmin)

9.
d = (1/(xmax - xmin)) (ymin xmax - xmin ymax)
m = (ymax - ymin) / (xmax - xmin)

In the following ymin = -1 and ymax = 1.
|#


(defun normalize-pt-unit-interval (pt min max)
  (declare (type real pt min max))

  (assert (and (< min max)
               (<= min pt max))     
      (pt min max))

  (let* ((range (- max min))
         (mx (/ 2.0 range)) ; -2.0 = (- (- 1 -1)); What was I thinking?
         (dx (* (/ range)
                ;; (- (* -1.0 max) (* min 1.0))
                (- (- max) min)
                ))
         )
    (declare (type real range mx dx))
    (+ (* mx pt) dx)
    ))


(defun normalize-points-1d (ptsx)
  ;; PTSX is an ordered vector.
  (declare (type (or list vector) ptsx))

  (assert (> (length ptsx) 1) (ptsx))

  (let* ((dxmax (elt ptsx (1- (length ptsx))))
         (dxmin (elt ptsx 0))
         (xwidth (- dxmax dxmin))

         (mx (- (/ -2.0 xwidth))
             ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
             )
         (dx (- (/ (- dxmin (- dxmax)) xwidth))
             ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
             )
         )

    (declare (type real dxmax dxmin xwidth mx dx))

    (flet ((norm-coord (x m d)
             (declare (type real x m d))
             (+ (* m x) d))
           )
      (declare (ftype (function (real real real) real) norm-coord))

      (map 'list #'(lambda (x) (declare (type real x)) (norm-coord x mx dx)) ptsx)
      )))


(defun normalize-points-2d (ptsx ptsy)
  ;; PTSX and PTSY are ordered vectors.
  (declare (type vector ptsx ptsy))
  (let* ((dxmax (aref ptsx (1- (length ptsx))))
         (dxmin (aref ptsx 0))
         (dymax (aref ptsy (1- (length ptsx))))
         (dymin (aref ptsy 0))
             
         (xwidth (- dxmax dxmin))
         (ywidth (- dymax dymin))
            
         (mx (- (/ -2.0 xwidth))
             ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
             )
         (dx (- (/ (- dxmin (- dxmax)) xwidth))
             ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
             )
       
         (my (- (/ -2.0 ywidth))
             ;; (- (/ (- -1.0 1.0) (- dymax dymin)))
             )
         (dy (- (/ (- dymin (- dymax)) ywidth))
             ;; (- (/ (- (* dymin 1.0) (* dymax -1.0)) (- dymax dymin)))
             )
         )

    (declare (type real
                   dxmax dxmin
                   dymax dymin
                   xwidth ywidth
                   mx dx my dy))

    (flet ((norm-coord (x m d)
             (declare (type real x m d))
             (+ (* m x) d))
           )
      (declare (ftype (function (real real real) real) norm-coord))

      (loop for x across ptsx
            for y across ptsy
            if (numberp y)
              collect (list (norm-coord x mx dx) (norm-coord y my dy))
            else
              collect (list (norm-coord x mx dx) :missing-value)
                )
      )))


(defmethod compute-normalized-points ((ds dataset-2d))
  ;; Essentially a scale and translation without the graphics
  ;; machinery. Think of the MODELVIEW matrix in OpenGL.

  (let* ((dxmax (dataset-x-max ds))
         (dxmin (dataset-x-min ds))
         (dymax (dataset-y-max ds))
         (dymin (dataset-y-min ds))
             
         (xwidth (- dxmax dxmin))
         (ywidth (- dymax dymin))
            
         (mx (- (/ -2.0 xwidth))
             ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
             )
         (dx (- (/ (- dxmin (- dxmax)) xwidth))
             ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
             )
       
         (my (- (/ -2.0 ywidth))
             ;; (- (/ (- -1.0 1.0) (- dymax dymin)))
             )
         (dy (- (/ (- dymin (- dymax)) ywidth))
             ;; (- (/ (- (* dymin 1.0) (* dymax -1.0)) (- dymax dymin)))
             )
         )

    (declare (type real
                   dxmax dxmin
                   dymax dymin
                   xwidth ywidth
                   mx dx my dy))

    (flet ((norm-coord (x m d)
             (declare (type real x m d))
             (+ (* m x) d))
           )
      (declare (ftype (function (real real real) real) norm-coord))

      (setf (dataset-normalized-points ds)
            (loop for x across (dataset-x-data ds)
                  for y across (dataset-y-data ds)
                  if (numberp y)
                    collect (list (norm-coord x mx dx) (norm-coord y my dy))
                  else
                    collect (list (norm-coord x mx dx) :missing-value)
                    )
            )))
  )


(defmethod compute-normalized-points ((ds dataset-3d))
  ;; Essentially a scale and translation without the graphics
  ;; machinery.  Think of the MODELVIEW matrix in OpenGL.

  (let* ((dxmax (dataset-x-max ds))
         (dxmin (dataset-x-min ds))

         (dymax (dataset-y-max ds))
         (dymin (dataset-y-min ds))

         (dzmax (dataset-z-max ds))
         (dzmin (dataset-z-min ds))
             
         (xwidth (- dxmax dxmin))
         (ywidth (- dymax dymin))
         (zwidth (- dzmax dzmin))
            
         (mx (- (/ -2.0 xwidth))
             ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
             )
         (dx (- (/ (- dxmin (- dxmax)) xwidth))
             ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
             )
       
         (my (- (/ -2.0 ywidth))
             ;; (- (/ (- -1.0 1.0) (- dymax dymin)))
             )
         (dy (- (/ (- dymin (- dymax)) ywidth))
             ;; (- (/ (- (* dymin 1.0) (* dymax -1.0)) (- dymax dymin)))
             )

         (mz (- (/ -2.0 zwidth))
             ;; (- (/ (- -1.0 1.0) (- dzmax dzmin)))
             )
         (dz (- (/ (- dzmin (- dzmax)) zwidth))
             ;; (- (/ (- (* dzmin 1.0) (* dzmax -1.0)) (- dzmax dzmin)))
             )
         )

    (declare (type real
                   dxmax dxmin
                   dymax dymin
                   dzmax dzmin
                   xwidth ywidth zwidth
                   mx dx my dy mz dz))

    (flet ((norm-coord (x m d)
             (declare (type real m d)
                      (type (or real symbol) x))
             (if (numberp x) (+ (* m (the real x)) d) :missing))
           )
      (declare (ftype (function ((or symbol real) real real)
                                (or (eql :missing) real))
                      norm-coord))

      (setf (dataset-normalized-points ds)
            (loop for x across (dataset-x-data ds)
                  for y across (dataset-y-data ds)
                  for z across (dataset-z-data ds)
                  collect (list (norm-coord x mx dx)
                                (norm-coord y my dy)
                                (norm-coord z mz dz))
                  ))
      )))


(defmethod dataset-normalized-points :before ((d dataset))
  (unless (and (slot-boundp d 'normalized-points)
               (slot-value d 'normalized-points))
    (compute-normalized-points d)))

  
;;;---------------------------------------------------------------------------
;;; Utilities.

#+nil
(defmacro do-points (graph &body forms)
  `(loop for x in (dataset-x-data ,graph)
         for y in (dataset-y-data ,graph)
         do (progn ,@forms)
         ))

(declaim (inline map-points))

(defun map-points (result-type f graph)
  (map result-type f (graph-dataset graph)))


(defun range (&optional (start 0) (end 0) (step 1))
  (loop for i from start below end by step
        collect i))


(defun data-index-vector (data)
  (declare (type array data))
  (make-array (array-dimension data 0)
              :element-type (array-element-type data)
              :initial-contents (range 0 (array-dimension data 0))))


(defun matrix-column (matrix j)
  (loop for i from 0 below (array-dimension matrix 0)
        collect (aref matrix i j)))


(defun matrix-row (matrix j)
  (loop for i from 0 below (array-dimension matrix 1)
        collect (aref matrix j i)))


;;; end of file -- dataset-implementation.lisp --
