;;; -*- Mode: Lisp -*-

(defpackage "IT.UNIMIB.DISCO.CL-PLOT" (:use "COMMON-LISP")
  (:nicknames "CL-PLOT" "LW-PLOT") ; Backeard compatibility.
  (:export
   "PLOT"
   "PLOT-DATA"
   )

  (:export
   "DEVICE")

  (:export
   "DATASET"
   "DATASET-2D"
   "DATASET-3D"

   "DATASETP"

   "MAKE-DATASET"

   "DATASET-NAME"

   "DATASET-X-DATA"
   "DATASET-Y-DATA"
   "DATASET-Z-DATA"

   "DATASET-X-MAX"
   "DATASET-Y-MAX"
   "DATASET-Z-MAX"

   "DATASET-X-MIN"
   "DATASET-Y-MIN"
   "DATASET-Z-MIN"

   "DATASET-NORMALIZED-POINTS"
   )

  (:export
   "BAR-DATASET")


  (:export
   "GRAPH"
   "GRAPH-2D"
   "GRAPH-3D"

   "GRAPHP"

   "MAKE-GRAPH"

   "GRAPH-NAME"

   "GRAPH-DATASETS"

   "GRAPH-X-DATA"
   "GRAPH-Y-DATA"
   "GRAPH-Z-DATA"

   "GRAPH-X-MAX"
   "GRAPH-Y-MAX"
   "GRAPH-Z-MAX"

   "GRAPH-X-MIN"
   "GRAPH-Y-MIN"
   "GRAPH-Z-MIN"

   "ADD-DATASET"
   "DELETE-DATASET"

   "X-LABELS"
   "Y-LABELS"
   "Z-LABELS"
   )
  )

;;; end of file -- cl-plot-package.lisp --
